$(document).ready(function(){


/* ==========================================================================
    Fullcalendar
    ========================================================================== */

    $('#calendar').fullCalendar({
        locale: 'ES',
        header: {
            left: '',
            center: 'prev, title, next',
            right: 'today agendaDay,agendaTwoDay,agendaWeek,month'
        },
        buttonIcons: {
            prev: 'font-icon font-icon-arrow-left',
            next: 'font-icon font-icon-arrow-right',
            prevYear: 'font-icon font-icon-arrow-left',
            nextYear: 'font-icon font-icon-arrow-right'
        },
        defaultView: 'agendaDay',
        editable: true,
        droppable: true,
        selectable: true,
        eventLimit: true, // allow "more" link when too many events
        events: '/api/consults/all',
        eventDrop: function(event, dayDelta, revertFunc) {
            start = event.start.format();
            end = event.end.format();
            axios.put('/api/consults/update/'+event.id, {start: start, end: end}).then(function(data){
                    $('.fc-popover.click').remove();
                    $('.fc-event').removeClass('event-clicked');
                    $('#calendar').fullCalendar( 'refetchEvents' );
                    flash('Se se modifico la cita!', 'info');
                })
        },
        viewRender: function(view, element) {
            if (!("ontouchstart" in document.documentElement)) {
                $('.fc-scroller').jScrollPane({
                    autoReinitialise: true,
                    autoReinitialiseDelay: 100
                });
            }

            $('.fc-popover.click').remove();
        },
        eventClick: function(calEvent, jsEvent, view) {

            var eventEl = $(this);
            // Add and remove event border class
            if (!$(this).hasClass('event-clicked')) {
                $('.fc-event').removeClass('event-clicked');

                $(this).addClass('event-clicked');
            }
            // Add popover
            $('body').append(
                '<div class="fc-popover click">' +
                    '<div class="fc-header">' +
                        moment(calEvent.start).format('dddd • D') +
                        '<button type="button" class="cl"><i class="font-icon-close-2"></i></button>' +
                    '</div>' +

                    '<div class="fc-body main-screen">' +
                        '<p>' +
                            moment(calEvent.start).format('dddd, D YYYY, hh:mma') +
                        '</p>' +
                        '<p class="color-blue-grey">'+calEvent.title+'</p>' +
                        '<p class="color-blue-grey">Comentario: '+calEvent.comment+'</p>' +
                        '<ul class="actions">' +
                            '<li><a href="#" class="fc-event-action-edit">Editar</a></li>' +
                            '<li><a href="#" class="fc-event-action-remove">Eliminar</a></li>' +
                            '<li><a href="/admin/patients/'+calEvent.patient.id+'" class="">Ver Paciente</a></li>' +
                        '</ul>' +
                    '</div>' +

                    '<div class="fc-body remove-confirm">' +
                        '<p>Desea eliminar esta cita?</p>' +
                        '<div class="text-center">' +
                            '<button type="button" class="btn btn-rounded btn-sm" id="delite-consult">Si</button>' +
                            '<button type="button" class="btn btn-rounded btn-sm btn-default remove-popover">No</button>' +
                        '</div>' +
                    '</div>' +

                    '<div class="fc-body edit-event">' +
                        '<p>Edit event</p>' +
                        '<div class="form-group">' +
                            '<div class="input-group date datetimepicker">' +
                                '<input type="text" class="form-control data-start-new" />' +
                                '<span class="input-group-addon"><i class="font-icon font-icon-calend"></i></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<textarea class="form-control comment" rows="2" placeholder="Comentario"></textarea>' +
                        '</div>' +
                        '<div class="text-center">' +
                            '<button type="button" class="btn btn-rounded btn-sm" id="updating">Guardar</button>' +
                            '<button type="button" class="btn btn-rounded btn-sm btn-default remove-popover">Cancelar</button>' +
                        '</div>' +
                    '</div>' +
                '</div>'
            );

            $('#updating').click(function(){
                start_new = $('.data-start-new').val();
                comment = $('.comment').val();
                axios.put('/api/consults/update/'+calEvent.id, {start: start_new, comment: comment}).then(function(data){
                    $('.fc-popover.click').remove();
                    $('.fc-event').removeClass('event-clicked');
                    $('#calendar').fullCalendar( 'refetchEvents' );
                    flash('Se se modifico la cita!', 'info');
                })
            });

            $('#delite-consult').click(function(){
                axios.delete('/api/consults/delete/'+calEvent.id).then(function(data){
                    $('.fc-popover.click').remove();
                    $('.fc-event').removeClass('event-clicked');
                    $('#calendar').fullCalendar( 'refetchEvents' );
                    flash('Se ha eliminado la cita.', 'danger');
                });
            });

            // Datepicker init
            $('.fc-popover.click .datetimepicker').datetimepicker({
                widgetPositioning: {
                    horizontal: 'right'
                }
            });

            $('.fc-popover.click .datetimepicker-2').datetimepicker({
                widgetPositioning: {
                    horizontal: 'right'
                },
                format: 'LT',
                debug: true
            });


            // Position popover
            function posPopover(){
                $('.fc-popover.click').css({
                    left: eventEl.offset().left + eventEl.outerWidth()/2,
                    top: eventEl.offset().top + eventEl.outerHeight()
                });
            }

            posPopover();

            $('.fc-scroller, .calendar-page-content, body').scroll(function(){
                posPopover();
            });

            $(window).resize(function(){
               posPopover();
            });


            // Remove old popover
            if ($('.fc-popover.click').length > 1) {
                for (var i = 0; i < ($('.fc-popover.click').length - 1); i++) {
                    $('.fc-popover.click').eq(i).remove();
                }
            }

            // Close buttons
            $('.fc-popover.click .cl, .fc-popover.click .remove-popover').click(function(){
                $('.fc-popover.click').remove();
                $('.fc-event').removeClass('event-clicked');
            });

            // Actions link
            $('.fc-event-action-edit').click(function(e){
                e.preventDefault();

                $('.fc-popover.click .main-screen').hide();
                $('.fc-popover.click .edit-event').show();
            });

            $('.fc-event-action-remove').click(function(e){
                e.preventDefault();
                $('.fc-popover.click .main-screen').hide();
                $('.fc-popover.click .remove-confirm').show();
            });
        }
    });


/* ==========================================================================
    Side datepicker
    ========================================================================== */

    $('#side-datetimepicker').datetimepicker({
        inline: true,
        format: 'DD/MM/YYYY'
    });

/* ========================================================================== */

});


/* ==========================================================================
    Calendar page grid
    ========================================================================== */

(function($, viewport){
    $(document).ready(function() {

        if(viewport.is('>=lg')) {
            $('.calendar-page-content, .calendar-page-side').matchHeight();
        }

        // Execute code each time window size changes
        $(window).resize(
            viewport.changed(function() {
                if(viewport.is('<lg')) {
                    $('.calendar-page-content, .calendar-page-side').matchHeight({ remove: true });
                }
            })
        );
    });
})(jQuery, ResponsiveBootstrapToolkit);






$(function() {
function cb(start, end) {
    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
}
cb(moment().subtract(29, 'days'), moment());

$('#daterange').daterangepicker({
    "timePicker": true,
    ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    "linkedCalendars": false,
    "autoUpdateInput": false,
    "alwaysShowCalendars": true,
    "showWeekNumbers": true,
    "showDropdowns": true,
    "showISOWeekNumbers": true
});


$('#daterange').on('show.daterangepicker', function(ev, picker) {
    /*$('.daterangepicker select').selectpicker({
        size: 10
    });*/
});

/* ==========================================================================
 Datepicker
 ========================================================================== */

 $('.datetimepicker-1').datetimepicker({
    widgetPositioning: {
        horizontal: 'right'
    },
    debug: false
});
});


