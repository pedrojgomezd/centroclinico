@extends('layouts.template')

@section('breadcrumb')
	<h3>Pacientes</h3>
	<ol class="breadcrumb breadcrumb-simple">
		<li><a href="/admin">Administrador</a></li>
		<li><a href="/admin/patientes">Pacientes</a></li>
		<li class="active">Ingresar</li>
	</ol>
@endsection

@section('content')
{{ Form::open(['route'=>'patients.store', 'files' => true]) }}

{{-- <div class="row">

	<div class="col-md-12">
		<div class="card card-inversed">
			<div class="card-header">
				Informacion personal
			</div>
			<div class="card-block">
				<div class="row">
					<div class="col-xs-2 profile-side-user">

						<button type="button" class="avatar-preview avatar-preview-128">
							<img src="/storage/avatars/avatar.jpg" alt="">
							<span class="update">
								<i class="font-icon font-icon-picture-double"></i>
								Seleccion foto!
							</span>
							<input type="file">
						</button>

					</div>
					<div class="col-md-10">
						
						<div class="row">
							<div class="col-xs-4">
								{!! Field::text('first_name') !!}
							</div>
							<div class="col-xs-4">
								{!! Field::text('last_name') !!}				
							</div>
							<div class="col-xs-4">
								{!! Field::date('birth') !!}
							</div>
						</div>
						<div class="row">
							<div class="col-xs-3">
								{!! Field::text('lugar_de_nacimiento') !!}				
							</div>
							<div class="col-xs-2">
								{!! Field::text('peso', ['label' => 'Peso (Kgs)']) !!}
							</div>
							<div class="col-xs-2">
								{!! Field::select('gender', ['male' => 'Masculino', 'female' => 'Femenino']) !!}
							</div>
							<div class="col-xs-2">
								{!! Field::text('ocupacion') !!}
							</div>
							<div class="col-xs-3">
								{!! Field::text('religion') !!}
							</div>
						</div>
						
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4">
						{!! Field::text('email') !!}
					</div>
					<div class="col-xs-4">
						{!! Field::text('phone_home') !!}
					</div>
					<div class="col-xs-4">
						{!! Field::text('mobil') !!}
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<h5 class="with-border m-t-lg">Direccion</h5>

						<div class="row">
							<div class="col-xs-5">
								{!! Field::text('colony') !!}
							</div>
							<div class="col-xs-4">
								{!! Field::text('city') !!}
							</div>
							<div class="col-xs-3">
								{!! Field::text('postal_code') !!}
							</div>
						</div>

						<div class="row">
							<div class="col-xs-8">
								{!! Field::text('calle') !!}
							</div>
							<div class="col-xs-4">
								{!! Field::text('number') !!}
							</div>
						</div>

					</div>

				</div>


			</div>
			<div class="card-footer">
				<button class="btn btn-default">Cancelar</button>
				<button class="btn btn-primary pull-right"><i class="fa fa-save"></i> Guardar</button>
			</div>
		</div>
	</div>

</div>
 --}}


{{ Form::close() }}

@endsection

@section('js')

<script>
	// $(function () {
	// 	places({
	// 		container: '#colony',
	// 		countries: ['mx'],
	// 		type: 'address'
	// 	}).on('change', function (e) {
	// 		console.log(e.suggestion)
	// 		console.log(e.suggestion.administrative)
	// 	})
	// })
</script>

@endsection