
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>{{ config('app.name') }}</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<link rel="stylesheet" href="startui/css/separate/pages/login.min.css">
    <link rel="stylesheet" href="startui/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="startui/css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="startui/css/main.css">
</head>
<body>

    <div class="page-center">
        <div class="page-center-in">
            <div class="container-fluid">
                <form class="sign-box" action="/login" method="POST">
                    {{ csrf_field() }}
                    <div class="sign-avatar">
                        <img src="/startui/img/avatar-sign.png" alt="">
                    </div>
                    <header class="sign-title">Iniciar Sesion</header>
                    
                    <fieldset class="form-group {{ $errors->has('email') ? ' form-group-error' : '' }}">
                        <label class="form-label" for="email">E-mail</label>
                        <div class="form-control-wrapper">
                            <input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-mail">
                             @if ($errors->has('email'))
                                <div class="form-tooltip-error">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                        </div>
                    </fieldset>

                    <fieldset class="form-group {{ $errors->has('password') ? ' form-group-error' : '' }}">
                        <label class="form-label" for="password">Contraseña</label>
                        <div class="form-control-wrapper">
                            <input type="password" name="password" class="form-control" id="password" placeholder="Contraseña">
                             @if ($errors->has('email'))
                                <div class="form-tooltip-error">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                        </div>
                    </fieldset>

                    <div class="form-group">
                        <div class="checkbox float-left">
                            <input type="checkbox" id="signed-in" name="remember" {{ old('remember') ? 'checked' : '' }} />
                            <label for="signed-in">Guardar datos!.</label>
                        </div>
                        <div class="float-right reset">
                            <a href="reset-password.html">Olvide mi contraseña!</a>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-rounded">Entrar</button>
                </form>
            </div>
        </div>
    </div><!--.page-center-->


<script src="startui/js/lib/jquery/jquery.min.js"></script>
<script src="startui/js/lib/tether/tether.min.js"></script>
<script src="startui/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="startui/js/plugins.js"></script>
    <script type="text/javascript" src="startui/js/lib/match-height/jquery.matchHeight.min.js"></script>
    <script>
        $(function() {
            $('.page-center').matchHeight({
                target: $('html')
            });

            $(window).resize(function(){
                setTimeout(function(){
                    $('.page-center').matchHeight({ remove: true });
                    $('.page-center').matchHeight({
                        target: $('html')
                    });
                },100);
            });
        });
    </script>
<script src="startui/js/app.js"></script>
</body>
</html>