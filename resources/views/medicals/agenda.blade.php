@extends('layouts.template')

@section('breadcrumb')
	<h3>Medico</h3>
	<ol class="breadcrumb breadcrumb-simple">
		<li><a href="/admin">Dashboard</a></li>
		<li class="active">Agenda</li>
	</ol>
@endsection

@section('content')
	<calendar></calendar>
@endsection