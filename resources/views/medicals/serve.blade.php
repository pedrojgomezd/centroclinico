@extends('layouts.template')

@section('breadcrumb')
	<h3>Medico</h3>
	<ol class="breadcrumb breadcrumb-simple">
		<li><a href="/admin">Dashboard</a></li>
		<li><a href="/admin">Consultas</a></li>
		<li class="active">Yadi Salome</li>
	</ol>
@endsection

@section('content')
<div class="row">
	<div class="col-md-7">
		<div class="card">
			<div class="card-header">
				Datos del paciente!
			</div>
			<div class="card-block">
				<div class="row">
					<div class="col-md-3 avatar-preview">
						<img src="/storage/avatars/pedrojgomezd.jpeg" alt="">
					</div>

					<div class="col-md-9">
						<div class="table-responsive">
							<table class="table table-hover table-bordered table-xs">
								<tbody>
									<tr>
										<th>Nombre y Apellido</th>
										<th>Edad</th>
										<th>Signo</th>
									</tr>
									<tr>
										<td>Pedro J Gomez</td>
										<td>27 anos</td>
										<td>Virgo</td>
									</tr>
									<tr>
										<th>Email</th>
										<th colspan="2">Telefonos</th>
									</tr>
									<tr>
										<td>pedrojgomezd@gmail.com</td>
										<td colspan="2">04146121298 / 04265985656</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-5">
		<div class="card">

			<div class="card-header">
				Chequeo basico.
			</div>
			<div class="card-block">
				
			</div>
		</div>
	</div>
</div>

@endsection