<div class="modal fade" id="modal-new-cita">
	<form 
		class="modal-dialog modal-lg"
		method="post" 
		action="/medical/consult" 
		>
		{{ csrf_field() }}
		<input type="hidden" name="medical_id" value="100">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Nueva cita</h4>
			</div>
			<div class="modal-body">

				<div class="container">								
					<div class="row">
						<div class="col-md-7">
							<div class="form-group">
								<div class='input-group'>
									<select class="select2" name="patient_id">
										<option value="">Seleccionar Paciente</option>
										@foreach($patients as $patient)
											<option value="{{ $patient->id }}">{{ $patient->full_name }}</option>
										@endforeach
									</select>
									<br/>
									<br/>
								</div>
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<div class='input-group date datetimepicker-1'>
									<input type='text' class="form-control" name="start" />
									<span class="input-group-addon">
										<i class="font-icon font-icon-calend"></i>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="submit" class="btn btn-primary">Agendar</button>
			</div>
		</div>
	</form>
</div>