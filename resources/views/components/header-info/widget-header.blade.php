<header class="page-content-header widgets-header">
	<div class="container-fluid">
		<div class="tbl tbl-outer">
			<div class="tbl-row">
				@foreach(['Consultas del mes', 'Consultas para hoy','Pacientes registrados'] as $label)
				<div class="tbl-cell">
					<div class="tbl tbl-item">
						<div class="tbl-row">
							<div class="tbl-cell">
								<div class="title">{{ $label }}</div>
								<div class="amount color-blue">{{ $consultsAll }}</div>
								<div class="amount-sm">{{ $consultsOpen }}</div>
							</div>
							<div class="tbl-cell tbl-cell-progress">
								<div 	class="circle-progress-bar-typical size-56 pieProgress pie_progress" 
										role="progressbar" 
										data-goal="79" 
										data-barcolor="#00a8ff"
										data-barsize="10" 
										aria-valuemin="0" 
										aria-valuemax="100" 
										aria-valuenow="79">
									<span class="pie_progress__number">100%</span>
									<div class="pie_progress__svg">
										<svg 	version="1.1" 
												preserveAspectRatio="xMinYMin meet" 
												viewBox="0 0 160 160">
											<ellipse 	rx="75" 
														ry="75" 
														cx="80" 
														cy="80" 
														stroke="#f2f2f2" 
														fill="none" 
														stroke-width="10">
											</ellipse>
												<path 	fill="none" 
														stroke-width="10" 
														stroke="#00a8ff" 
														d="M80,5 A75,75 0 1 1 7.356262915352673,61.34825846263588" 
														style="stroke-dasharray: 372.292, 372.292; stroke-dashoffset: 0;">
												</path>
										</svg>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</header>