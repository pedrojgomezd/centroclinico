<div class="card @isset ($class) {{ $class }} @endisset">
	<div class="card-header">
		{!! $title ?? ''!!}
	</div>
	@if(isset($slot) && !isset($table))
		<div class="card-block">
			{{ $slot }}
		</div>
	@endif
	
	<div class="table-content">
		{{ $table ?? '' }}		
	</div>

	{{ $foot ?? '' }}
</div>