<div class="card">
	<div class="card-header">
		Datos del Usuario.
	</div>
	<div class="card-block">
		<h5 class="with-border">Información personal.</h5>
		<div class="row">
			<div class="col-lg-3">
				{!! Field::text('first_name', optional($user)->first_name) !!}
			</div>
			<div class="col-lg-3">
				{!! Field::text('last_name', optional($user)->last_name) !!}
			</div>
			<div class="col-lg-2">
				{!! Field::select('roles', ['admin' => 'Administrador', 'medical' => 'Medico', 'reception' => 'Recepcion']) !!}
			</div>
			<div class="col-lg-2">
				{!! Field::select('gender', ['male' => 'Masculino', 'female' => 'Femenino'], optional($user)->gender) !!}
			</div>
			<div class="col-lg-2">
				{!! Field::date('birth', optional($user)->birth) !!}
			</div>
		</div>
		<h5 class="with-border">Datos de Accesos.</h5>
		<div class="row">
			<div class="col-lg-4">
				{!! Field::text('username', optional($user)->username) !!}
			</div>
			<div class="col-lg-4">
				{!! Field::text('password') !!}
			</div>
			<div class="col-lg-4">
				{!! Field::text('password_confirmation') !!}
			</div>
		</div>
		@include('users.includes.form-profile')
	</div>
</div>