@if(request()->path() == 'admin/users/create')
<h5 class="with-border">Datos de contacto.</h5>
<div class="row">
	<div class="col-lg-3">
		{!! Field::text('phone_home') !!}
	</div>
	<div class="col-lg-3">
		{!! Field::text('phone_office') !!}
	</div>
	<div class="col-lg-3">
		{!! Field::text('mobil') !!}
	</div>
	<div class="col-lg-3">
		{!! Field::text('email') !!}
	</div>
</div>
<h5 class="with-border">Foto de perfil.</h5>
<div class="row">
	<div class="col-md-4">
		{!! Field::file('avatar') !!}
	</div>
</div>
@else

<h5 class="with-border">Datos de contacto.</h5>
<div class="row">
	<div class="col-lg-3">
		{!! Field::text('phone_home', optional($user)->profile->phone_home) !!}
	</div>
	<div class="col-lg-3">
		{!! Field::text('phone_office', optional($user)->profile->phone_office) !!}
	</div>
	<div class="col-lg-3">
		{!! Field::text('mobil', optional($user)->profile->mobil) !!}
	</div>
	<div class="col-lg-3">
		{!! Field::text('email', optional($user)->email) !!}
	</div>
</div>
<h5 class="with-border">Foto de perfil.</h5>
<div class="row">
	<div class="col-md-4">
		{!! Field::file('avatar') !!}
	</div>
</div>

@endif