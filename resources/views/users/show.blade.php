@extends('layouts.template')

@section('breadcrumb')
	<h3>Usuarios</h3>
	<ol class="breadcrumb breadcrumb-simple">
		<li><a href="/admin">Dashboard</a></li>
		<li><a href="/admin/users">Medicos</a></li>
		<li class="active">{{ $user->full_name }}</li>
	</ol>
@endsection

@section('content')

<div class="row">
	<div class="col-md-3">
		@component('components.card-component')
			@slot('title', $user->full_name." <a href='/admin/users/{$user->username}/edit' class='btn btn-success btn-sm'><i class='fa fa-edit'></i></a>")
			
			@slot('table')
			<img src="/storage/avatars/{{ $user->avatar_path }}" width="100%" alt="">
			@endslot

			@slot('foot')
				<ul class="profile-links-list">
					<li class="nowrap">
						<i class="font-icon font-icon-user"></i>
						Medico
					</li>
					<li class="nowrap">
						<i class="font-icon font-icon-phone"></i>
						{{ $user->profile->mobil }}
					</li>
					<li class="nowrap">
						<i class="font-icon font-icon-mail"></i>
						{{ $user->email }}
					</li>
				</ul>
			@endslot
		@endcomponent

	</div>
	<div class="col-md-9">
	
		@component('components.card-component')
			@slot('title', 'Citas con pacientes')

			@slot('class', 'card-blue')
			
			@slot('table')

				<table class="table table-hover">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Edad</th>
							<th>Sexo</th>
							<th>Fecha de Cita</th>
							<th>Accion</th>
						</tr>
					</thead>
					<tbody>
					@forelse($user->my_consults as $consult)
						<tr>
							<td>{{ $consult->patient->full_name }}</td>
							<td>{{ $consult->patient->age }} año</td>
							<td>{{ $consult->patient->gender }}</td>
							<td>{{ $consult->start }}</td>
							<td>
								<button class="btn btn-success btn-sm">
									Atender
								</button>

								<button class="btn btn-default btn-sm">
									<i class="fa fa-eye"></i>
								</button>
							</td>
						</tr>
					@empty
						<tr>
							<td colspan="6">
								No tiene citas programadas.
							</td>
						</tr>
					@endforelse
					</tbody>
				</table>

			@endslot
		@endcomponent
	</div>
</div>
@endsection