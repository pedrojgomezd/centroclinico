@extends('layouts.template')

@section('breadcrumb')
	<h3>Editar usuarios</h3>
	<ol class="breadcrumb breadcrumb-simple">
		<li><a href="/admin">Administrador</a></li>
		<li><a href="/admin/users">Usuarios</a></li>
		<li class="active">Editar</li>
	</ol>
@endsection

@section('content')

<form-user :user=" {{ $user }} "></form-user>

@endsection
