@extends('layouts.template')

@section('breadcrumb')
	<h3>Usuarios</h3>
	<ol class="breadcrumb breadcrumb-simple">
		<li><a href="/admin">Administrador</a></li>
		<li class="active">Usuarios</li>
	</ol>

@endsection

@section('content')


<section class="box-typical">
	<header class="box-typical-header">
		<div class="tbl-row">
			<div class="tbl-cell tbl-cell-title">
				<h3>{{ $users->count() }} Usuarios registrados</h3>
			</div>
			<div class="tbl-cell tbl-cell-actions">
				<a href="/admin/users/create" class="btn btn-success-outline btn-sm">
					<i class="fa fa-user-plus"></i>
					Nuevo
				</a>
			</div>
		</div>
	</header>
	<div class="box-typical-body">
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th></th>
						<th>Nombre</th>
						<th>Cargo</th>
						<th>Email</th>
						<th>Celular</th>
						<th>Accion</th>
					</tr>
				</thead>
				<tbody>
				@foreach($users as $user)
					<tr>
						<td class="table-photo">
							<img src="/storage/avatars/{{ $user->avatar_path }}" alt="" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="{{ $user->full_name }}">
						</td>
						<td>
							<a href="/admin/users/{{ $user->username }}">
								{{ $user->full_name }}
							</a>
						</td>
						<td>{{ __($user->roles) }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->profile->mobil }}</td>
						<td>
							<div class="btn-group">
								<a href="{{ route('users.edit', $user) }}" 
											class="btn btn-info btn-sm">
										<span class="fa fa-edit"></span>
								</a>
								<a href="#" 
											class="btn btn-success btn-sm">
									<span class="fa fa-eye"></span>
								</a>
							</div>
                        </td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div><!--.box-typical-body-->
</section>

@endsection
