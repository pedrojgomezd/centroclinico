@extends('layouts.template')

@section('breadcrumb')
	<h3>Usuarios</h3>
	<ol class="breadcrumb breadcrumb-simple">
		<li><a href="/admin">Administrador</a></li>
		<li><a href="/admin/users">Usuarios</a></li>
		<li class="active">Nuevo</li>
	</ol>
@endsection

@section('content')

<form-user :user="{ avatar: null }"></form-user>

@endsection
