@extends('layouts.template')

@section('breadcrumb')
	<h3>Dashboard</h3>
	<ol class="breadcrumb breadcrumb-simple">
		<li><a href="/reception">{{ __('reception') }}</a></li>
	</ol>
@endsection

@section('content')
@section('header-info')
	@include('components.header-info.widget-header')
@endsection
	<citas></citas>
@endsection