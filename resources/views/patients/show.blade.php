@extends('layouts.template')

@section('breadcrumb')
	<h3>Expediente</h3>
	<ol class="breadcrumb breadcrumb-simple">
		<li><a href="/admin">Dashboard</a></li>
		<li><a href="/admin/users">Expediente</a></li>
	</ol>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-4">
			<section class="box-typical">
				<div class="profile-card">
					<div class="profile-card-photo">
						<img src="/storage/avatars/{{ $patient->avatar }}" alt="">
					</div>
					<div class="profile-card-name">{{ $patient->full_name }}</div>
					<div class="profile-card-status">{{ __($patient->gender) }}</div>
					<div class="profile-card-status">{{ $patient->age }} ano ({{__($patient->zodiac->name)}})</div>
					<div class="profile-card-location">Cabimas</div>
					<a href="/admin/patients/{{ $patient->id }}/edit" class="btn btn-rounded">Editar</a>
				</div><!--.profile-card-->

				{{-- <div class="profile-statistic tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<b>200</b>
							Connections
						</div>
						<div class="tbl-cell">
							<b>1.9M</b>
							Followers
						</div>
					</div>
				</div> --}}

				<ul class="profile-links-list">
					<li class="divider"></li>
					<li class="nowrap">
						<i class="font-icon font-icon-phone"></i>
						{{ $patient->mobil }} - {{ $patient->phone_home }}
					</li>
					<li class="nowrap">
						<i class="font-icon font-icon-mail"></i>
						<a href="#">{{ $patient->email }}</a>
					</li>
					<li class="nowrap">
						<i class="font-icon font-icon-earth-bordered"></i>
						{{ $patient->address->state }} - {{ $patient->address->city }}
					</li>
					<li class="nowrap">
						<i class="font-icon font-icon-earth-bordered"></i>
						{{ $patient->address->street }}
					</li>
					{{-- <li class="divider"></li> --}}
					{{-- <li>
						<i class="font-icon font-icon-pdf-fill"></i>
						<a href="#">Export page as PDF</a>
					</li> --}}
				</ul>
			</section>
		</div>

		<div class="col-md-8">
			
			<table-citas :patient_id="{{ $patient->id }}"></table-citas>

			@component('components.card-component')
					@slot('title', 'Historias Clinicas')
				
					@slot('class', 'card-yelow')
					
					@slot('table')
				
						<table class="table table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Fecha de consulta</th>
									<th>Medico</th>
									<th>Accion</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>81239</td>
									<td>23/07/2017</td>
									<td>Jose Lujan</td>
									<td>
										<button class="btn btn-info btn-sm">
											<i class="fa fa-eye"></i>
										</button>
									</td>
								</tr>
							</tbody>
						</table>
				
					@endslot
				@endcomponent
		</div>
	</div>
@endsection