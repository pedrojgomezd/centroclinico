@extends('layouts.template')

@section('breadcrumb')
	<h3>Pacientes</h3>
	<ol class="breadcrumb breadcrumb-simple">
		<li><a href="/admin">Administrador</a></li>
		<li><a href="/admin/patientes">Pacientes</a></li>
		<li class="active">Ingresar</li>
	</ol>
@endsection

@section('content')
{{ Form::open([ 'route'=>'patients.store', 'files' => true ]) }}

<form-patient :patient="{avatart: null}"></form-patient>

{{ Form::close() }}

@endsection

