@extends('layouts.template')

@section('breadcrumb')
	<h3>Pacientes</h3>
	<ol class="breadcrumb breadcrumb-simple">
		<li><a href="/admin">Administrador</a></li>
		<li><a href="/admin/patientes">Pacientes</a></li>
		<li class="active">Lista</li>
	</ol>
@endsection

@section('content')

<table-patient></table-patient>

{{-- 
<section class="box-typical">
	<header class="box-typical-header">
		<div class="tbl-row">
			<div class="tbl-cell tbl-cell-title">
				<h3>{{ $patients->count() }} Pacientes registrados</h3>
			</div>
			<div class="tbl-cell tbl-cell-actions">
				<a href="/admin/patients/create" class="btn btn-success-outline">
					<i class="fa fa-user-plus"></i>
					Nuevo Paciente
				</a>
			</div>
		</div>
	</header>
	<div class="box-typical-body">
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th></th>
						<th>Nombre</th>
						<th>Edad</th>
						<th>Sexo</th>
						<th>Signo</th>
						<th>Celular</th>
						<th>Email</th>
						<th>Accion</th>
					</tr>
				</thead>
				<tbody>
				@foreach($patients as $patient)
					<tr>
						<td class="table-photo">
							<img src="/storage/avatars/{{ $patient->avatar }}" alt="" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="{{ $patient->full_name }}">
						</td>
						<td>
							<a href="{{ route('patients.show', $patient) }}">
								{{ $patient->full_name }}
							</a>
						</td>
						<td>{{ $patient->age }}</td>
						<td>{{ __($patient->gender) }}</td>
						<td>{{ $patient->zodiac }}</td>
						<td>{{ $patient->mobil }}</td>
						<td>{{ $patient->email }}</td>
						<td style="white-space: nowrap; width: 1%;">
							<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
                               <div class="btn-group btn-group-sm" style="float: none;">
	                               	<a href="{{ route('patients.edit', $patient) }}" 
	                               			class="btn btn-sm btn-default" 
	                               			style="float: none;">
	                               		<span class="fa fa-edit"></span>
	                               	</a>
	                               	<a href="{{ route('patients.show', $patient) }}" 
	                               			class="btn btn-sm btn-default" 
	                               			style="float: none;">
	                               		<span class="fa fa-eye"></span>
	                               	</a>
                               	</div>
                           </div>
                        </td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div><!--.box-typical-body-->
</section>
 --}}
@endsection