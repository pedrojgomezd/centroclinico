@extends('layouts.template')

@section('breadcrumb')
	<h3>Editar pacientes</h3>
	<ol class="breadcrumb breadcrumb-simple">
		<li><a href="/admin">Administrador</a></li>
		<li><a href="/admin/patientes">Pacientes</a></li>
		{{-- <li class="active">{{ $patient->full_name }}</li> --}}
	</ol>
@endsection

@section('content')

<form-patient :patient="{{ $patient }}"></form-patient>

@endsection
