<fieldset  id="field_{{ $id }}"{!! Html::classes(['form-group', 'form-group-error' => $hasErrors]) !!}>
    <label for="{{ $id }}" class="form-label">
        {{ $label }}
    </label>

    @if ($required)
        <span class="label label-info">Required</span>
    @endif

    <div class="controls">
        {!! $input !!}
        @foreach ($errors as $error)
            <p class="help-block">{{ $error }}</p>
        @endforeach
    </div>
</fieldset>