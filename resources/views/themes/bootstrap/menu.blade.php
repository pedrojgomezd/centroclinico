<div class="mobile-menu-left-overlay"></div>
<nav class="side-menu">
    <ul class="side-menu-list">
    @foreach($items as $item)
        
        @if (empty($item['submenu']))
        <li @if ($item['class']) class="{{ $item['class'] }}" @endif id="menu_{{ $item['id'] }}">
            <a href="{{ $item['url'] }}">
                <span class="{{ $item['icon'] }}"></span>
                <span class="lbl">{{ $item['title'] }}</span>
            </a>
        </li>
        @else
        <li @if ($item['class']) class="{{ $item['class'] }} with-sub" @else class="with-sub" @endif id="menu_{{ $item['id'] }}">
            <span>
                <i class="{{ $item['icon'] }}"></i>
                <span class="lbl">{{ $item['title'] }}</span>
            </span>
            <ul>
                @foreach ($item['submenu'] as $subitem)
                <li><a href="{{ $subitem['url'] }}"><span class="lbl">{{ $subitem['title'] }}</span></a></li>
                @endforeach
            </ul>
        </li>
        @endif
        @endforeach
    </ul>
</nav><!--.side-menu-->