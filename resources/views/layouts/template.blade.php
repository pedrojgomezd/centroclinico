<!DOCTYPE html>
<html>
<head lang="en">
	@include('layouts.partials.head')
</head>
<body class="with-side-menu">

<div id="app">
	@include('layouts.partials.header')
		
	@include('layouts.partials.nav')
		
		<div class="page-content">

			@yield('header-info')

			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							@yield('breadcrumb')
						</div>
					</div>
				</div>
			</header>

			<div class="container-fluid">
				@yield('content')
			</div><!--.container-fluid-->
				

		</div><!--.page-content-->
<flash message="{{ session('flash') }}"></flash>
</div>


@include('layouts.partials.script')
</body>
</html>