<header class="site-header">
	<div class="container-fluid">

		<a href="#" class="site-logo">
			<img class="hidden-md-down" src="/images/clinico-logo.png" alt="">
			<img class="hidden-lg-up" src="/images/clinico-logo-mob.png" alt="">
		</a>

		<button id="show-hide-sidebar-toggle" class="show-hide-sidebar">
			<span>toggle menu</span>
		</button>

		<button class="hamburger hamburger--htla">
			<span>toggle menu</span>
		</button>
		<div class="site-header-content">
			<div class="site-header-content-in">
				<div class="site-header-shown">
					<div class="dropdown dropdown-notification notif">
						<a href="#"
						class="header-alarm dropdown-toggle active"
						id="dd-notification"
						data-toggle="dropdown"
						aria-haspopup="true"
						aria-expanded="false">
							<i class="font-icon-alarm"></i>
						</a>
						<notification></notification>					

					</div>

				<div class="dropdown dropdown-notification messages">
					<a href="#"
					class="header-alarm dropdown-toggle active"
					id="dd-messages"
					data-toggle="dropdown"
					aria-haspopup="true"
					aria-expanded="false">
					<i class="font-icon-mail"></i>
					</a>
				<div class="dropdown-menu dropdown-menu-right dropdown-menu-messages" aria-labelledby="dd-messages">
					<div class="dropdown-menu-messages-header">
						<ul class="nav" role="tablist">
							<li class="nav-item">
								<a class="nav-link active"
								data-toggle="tab"
								href="#tab-incoming"
								role="tab">
								Inbox
								<span class="label label-pill label-danger">8</span>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link"
								data-toggle="tab"
								href="#tab-outgoing"
								role="tab">Outbox</a>
							</li>
						</ul>
					</div>
				<div class="tab-content">
					<div class="tab-pane active" id="tab-incoming" role="tabpanel">
						<div class="dropdown-menu-messages-list">
							<a href="#" class="mess-item">
								<span class="avatar-preview avatar-preview-32"><img src="/startui/img/photo-64-2.jpg" alt=""></span>
								<span class="mess-item-name">Tim Collins</span>
								<span class="mess-item-txt">Morgan was bothering about something!</span>
							</a>
						</div>
					</div>
					<div class="tab-pane" id="tab-outgoing" role="tabpanel">
						<div class="dropdown-menu-messages-list">
							<a href="#" class="mess-item">
								<span class="avatar-preview avatar-preview-32"><img src="/startui/img/avatar-2-64.png" alt=""></span>
								<span class="mess-item-name">Christian Burton</span>
								<span class="mess-item-txt">Morgan was bothering about something! Morgan was bothering about something...</span>
							</a>
							<a href="#" class="mess-item">
								<span class="avatar-preview avatar-preview-32"><img src="/startui/img/photo-64-2.jpg" alt=""></span>
								<span class="mess-item-name">Tim Collins</span>
								<span class="mess-item-txt">Morgan was bothering about something! Morgan was bothering about something.</span>
							</a>
							<a href="#" class="mess-item">
								<span class="avatar-preview avatar-preview-32"><img src="/startui/img/avatar-2-64.png" alt=""></span>
								<span class="mess-item-name">Christian Burtons</span>
								<span class="mess-item-txt">Morgan was bothering about something!</span>
							</a>
							<a href="#" class="mess-item">
								<span class="avatar-preview avatar-preview-32"><img src="/startui/img/photo-64-2.jpg" alt=""></span>
								<span class="mess-item-name">Tim Collins</span>
								<span class="mess-item-txt">Morgan was bothering about something!</span>
							</a>
						</div>
					</div>
				</div>
				<div class="dropdown-menu-notif-more">
					<a href="#">See more</a>
				</div>
			</div>
		</div>
		<div class="dropdown user-menu">
			<button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				{{ auth()->user()->full_name }}
				<img src="/storage/avatars/{{ auth()->user()->avatar_path }}" alt="">
			</button>
			<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
				<a class="dropdown-item" href="#"><span class="font-icon glyphicon glyphicon-user"></span>Profile</a>
				<a class="dropdown-item" href="#"><span class="font-icon glyphicon glyphicon-cog"></span>Settings</a>
				<a class="dropdown-item" href="#"><span class="font-icon glyphicon glyphicon-question-sign"></span>Help</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="{{ route('logout') }}"
                                         onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                    <span class="font-icon glyphicon glyphicon-log-out"></span>Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
			</div>
		</div>

		<button type="button" class="burger-right">
			<i class="font-icon-menu-addl"></i>
		</button>
	</div><!--.site-header-shown-->

</div><!--site-header-content-in-->
</div><!--.site-header-content-->
</div><!--.container-fluid-->
</header><!--.site-header-->