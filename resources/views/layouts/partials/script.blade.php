		<!--
	-->
	<script src="/startui/js/lib/jquery/jquery.min.js"></script>

	<script src="/startui/js/lib/tether/tether.min.js"></script>
	<script src="/startui/js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="/startui/js/plugins.js"></script>

	<script type="text/javascript" src="/startui/js/lib/match-height/jquery.matchHeight.min.js"></script>
	<script type="text/javascript" src="/startui/js/lib/moment/moment-with-locales.min.js"></script>
	<script src="/startui/js/lib/fullcalendar/fullcalendar.min.js"></script>
	<script type="text/javascript" src="/startui/js/lib/eonasdan-bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
	<script src="/startui/js/lib/daterangepicker/daterangepicker.js"></script>
	<script src="/startui/js/lib/select2/select2.full.min.js"></script>

	<script src="/startui/js/app.js"></script>
	<script src="/js/core.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.0/lang/es.js"></script>
	
	@yield('js')