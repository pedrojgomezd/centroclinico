<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{{ config('app.name') }}</title>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<link rel="stylesheet" href="/startui/css/main.css">
<![endif]-->
<link rel="stylesheet" href="/startui/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="/startui/css/separate/vendor/select2.min.css"">
<link rel="stylesheet" href="/startui/css/lib/bootstrap/bootstrap.min.css">

<link rel="stylesheet" href="/startui/css/lib/fullcalendar/fullcalendar.min.css">
<link rel="stylesheet" href="/startui/css/separate/pages/calendar.min.css">
<link rel="stylesheet" href="/css/app2.css">

<link rel="stylesheet" href="/startui/css/separate/pages/profile.min.css">
<link rel="stylesheet" href="/startui/css/separate/pages/profile-2.min.css">

<link rel="stylesheet" href="/startui/css/separate/vendor/bootstrap-datetimepicker.min.css">

<meta name="csrf-token" content="{{ csrf_token() }}">
