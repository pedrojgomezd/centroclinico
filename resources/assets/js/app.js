
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueSweetalert2 from 'vue-sweetalert2';


//require('./feature/FormUser');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.use(VueSweetalert2);

Vue.component('notification', require('./components/notifications/notification.vue'));
Vue.component('flash', require('./components/Flash.vue'));
Vue.component('citas', require('./components/reception/Citas.vue'));

/**
 * Features RESOURCE
 */
Vue.component('form-patient', require('./components/feature/FormPatient.vue'));
Vue.component('table-patient', require('./components/feature/patient/TablePatient.vue'));
Vue.component('table-citas', require('./components/feature/patient/TableCitas.vue'));

Vue.component('form-user', require('./components/feature/FormUser.vue'));


const app = new Vue({
    el: '#app',   
});

