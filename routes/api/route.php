<?php 

Route::post('medicals', [
        'uses' => 'Api\ApiController@medicals',
        'as' => 'api.medicals'
    ]);
Route::post('medical/{username}', [
        'uses' => 'Api\ApiController@medical',
        'as' => 'api.medical'
    ]);
Route::post('medicals/consult/{user}/add', [
        'uses' => 'Api\ApiController@newConsulta',
        'as' => 'new.consult'
    ]);

Route::group(['prefix' => 'consults'], function() {
	Route::get('all', function() {
		$consults =\App\Models\Consult::all();
        $consults->load('patient');
	    return response()->json($consults);
	});

	Route::get('auth', function() {
	    return auth()->user()->consults;
	});

    Route::put('update/{consult}', [
        'uses' => 'ConsultController@update',
        'as' => 'consult.update'
    ]);

    Route::delete('delete/{consult}','ConsultController@destroy');
});

Route::group(['prefix' => 'notifications'], function() {
    Route::get('unread', function() {
        return auth()->user()->unreadNotifications;
    });
});