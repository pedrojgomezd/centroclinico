<?php

Auth::routes();

Route::redirect('/', '/reception/dahsboard')->middleware('auth');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {  
    Route::resource('users', 'UsersController');
    Route::resource('patients', 'PatientController');
});

Route::group(['prefix' => 'reception', 'middleware' => 'auth'], function () {
    Route::view('dahsboard', 'reception.dashboard')->name('reception.dashboard');
});

Route::group(['prefix' => 'medical', 'middleware' => 'auth'], function() {
    Route::view('/agenda', 'medicals.agenda')->name('medicals.agenda');
    Route::view('/serve', 'medicals.serve');
});

Route::group(['prefix' => 'api'], function () {
    Route::resource('consults', 'ConsultController');
    Route::get('medicals', 'MedicalController@index');
    
    Route::post('/patients', 'PatientController@fetech');
    Route::put('/patients/{patient}', 'PatientController@update');
    Route::get('/patients/{patient}/show', 'PatientController@show');
    Route::post('/patients/avatar/{patient}', 'PatientController@avatar');

    Route::post('/users', 'UsersController@store');
    Route::put('/users/{username}', 'UsersController@update');
    Route::post('/users/avatar/{username}', 'UsersController@avatar');
});
