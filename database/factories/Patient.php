<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Patient::class, function (Faker $faker) {
    return [
            "first_name" => $faker->firstName,
            "last_name" => $faker->lastName,
            "peso" => $faker->numberBetween(15, 123),
            "gender" => $faker->randomElement(['female', 'male']),
            "birth" => $faker->date,
            "lugar_de_nacimiento" => $faker->country,
            "ocupacion" => 'Estudiante',
            "signo_zodiacal" => 'Aries',
            "religion" => 'SUD',
            "representante" => $faker->name,
            "phone_home" => $faker->phoneNumber,
            "phone_office" => $faker->phoneNumber,
            "mobil" => $faker->phoneNumber,
            "email" => $faker->email,
            "avatar" => 'avatar.jpg'
    ];
});
