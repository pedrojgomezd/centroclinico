<?php

use Faker\Generator as Faker;

$factory->define(App\Profile::class, function (Faker $faker) {
    return [

        'user_id' => factory(App\User::class)->create()->id,
		'phone_home' => '04146121298',
		'phone_office' => '0212323234',
		'mobil' => '04162689968',
    ];
});
