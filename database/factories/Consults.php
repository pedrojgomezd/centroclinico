<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Models\Consult::class, function (Faker $faker) {
    return [
        'medical_id' => factory(App\Profile::class)->create()->user->id,
        'patient_id' => factory(App\Models\Patient::class)->create()->id, 
        'created_by_id' => 1,
        'start' => Carbon::now(), 
    ];
});
