<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consults', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('medical_id');
            $table->unsignedInteger('patient_id');
            $table->unsignedInteger('created_by');

            $table->dateTime('start');

            $table->enum('statu', ['open', 'cancel', 'ready'])
                ->default('open');

            $table->text('comment')
                ->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consults');
    }
}
