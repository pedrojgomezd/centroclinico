<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('weight', 4);
            $table->enum('gender', ['male', 'female']);
            $table->date('birthday');
            $table->string('place_of_birth');
            $table->string('job');
            $table->string('religion');
            $table->string('email');
            $table->string('phone_home')->nullable();
            $table->string('mobil');
            $table->string('avatar')->default('avatar.png');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
