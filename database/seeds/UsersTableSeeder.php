<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([
        		'username' => 'PedrojGomez',
        		'first_name' => 'Pedro J',
        		'last_name' => 'Gomez',
        		'email' => 'pedrojgomezd@gmail.com',
        	])->addresses()->create([
                'country' => 'Venezuela',
                'state' => 'Zulia',
                'city' => 'Caimas',
                'post_code' => '4013',
                'street' => 'Los LAureles Calle 25',
                'number' => '10',
            ]);
    }
}
