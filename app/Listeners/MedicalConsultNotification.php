<?php

namespace App\Listeners;

use App\Events\RegisterConsult;
use App\Notifications\NewConsult;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class MedicalConsultNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisterConsult  $event
     * @return void
     */
    public function handle(RegisterConsult $event)
    {
        $event->consult->medical
            ->notify(new NewConsult($event->consult));
    }
}
