<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Filters\ConsultsFilters;

class Consult extends Model
{
    protected $fillable = [	
                            'medical_id', 	'patient_id', 
                            'created_by', 	'start', 
                            'statu', 
    					];

    protected static function boot()
    {
        parent::boot();

        static::updating(function ($model) {
            $model->start = new \Carbon\Carbon(request()->start);
        });

        // static::craeting(function ($model) {
        //     $model->start = new \Carbon\Carbon(request()->start);
        // });
    }


    public function create_by()
    {
    	return $this->belongsTo(\App\User::class, 'created_by');
    }

    public function patient()
    {
    	return $this->belongsTo(Patient::class);
    }

    public function medical()
    {
    	return $this->belongsTo(\App\User::class, 'medical_id');
    }

    public function getStartAttribute($value)
    {
        return new \Carbon\Carbon($value);
    }

    public function scopeFilter($query, ConsultsFilters $filters)
    {
        return $filters->apply($query);
    }

}
