<?php

namespace App\Models;

use Clinico\Address\Addressable;
use Illuminate\Database\Eloquent\Model;
use Intervention\Zodiac\EloquentZodiacTrait;

class Patient extends Model
{
    use EloquentZodiacTrait, Addressable;

    protected $fillable = [
    			"first_name",		"last_name",
                "weight",			"gender",
                "birthday",			"place_of_birth",
                "job",		        "religion",			
                "phone_home",		"mobil",
                "email",            "avatar"
              ];

    public function consults()
    {
        return $this->hasMany(Consult::class);
    }

    public function getFullNameAttribute()
    {
    	return $this->first_name.' '.$this->last_name;
    }

    public function getAgeAttribute()
    {
        return (new \Carbon\Carbon($this->birthday))->age;
    }

    public function updateChange($request)
    {
        $this->update($request);
        
        $this->addresses()
                ->first()
                ->update($request);
    }

}
