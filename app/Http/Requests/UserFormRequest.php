<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'      => 'required|string|max:15|unique:users,id,:id',
            'first_name'    => 'required|string|min:3|max:35',
            'last_name'     => 'required|string|min:3|max:35',
            'roles'         => 'required',
            'gender'        => 'required',
            'birthday'      => 'required',
            'email'      => 'required|string|email|max:255|unique:users,id,:id',
            'mobil'         => 'required',
            //'password'      => 'required|string|min:6|confirmed',
            'state'         => 'required',
            'city'          => 'required',
            'post_code'     => 'required',
            'street'        => 'required',
            'number'        => '',
            'avatar'        => '',

        ];
    }
}
