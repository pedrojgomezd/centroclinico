<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatientFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;//auth()->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "first_name"     => "required",
            "last_name"      => "required",
            "weight"         => "required",
            "gender"         => "required",
            "birthday"       => "required",
            "place_of_birth" => "required",
            "job"            => "required",
            "religion"       => "required",
            "phone_home"     => "",
            "mobil"          => "required",
            "email"          => "required",
            "state"          => "required",
            "city"           => "required",
            "post_code"      => "required",
            "street"         => "required",
            "number"         => "",
        ];
    }
}
