<?php

namespace App\Http\Resources;

use App\Http\Resources\ConsultPatientResource;
use Illuminate\Http\Resources\Json\Resource;

class PatientResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'full_name' => $this->fullName,
            'gender' => __($this->gender),
            'zodiac' => __($this->zodiac->name),
            'birthday' => $this->age,
            'mobil' => $this->mobil,
            'email' => $this->email,
            'avatar' => '/storage/avatars/'.$this->avatar,
            'address' => $this->address,
            'consults' => ConsultPatientResource::collection($this->consults)
        ];
    }
}
