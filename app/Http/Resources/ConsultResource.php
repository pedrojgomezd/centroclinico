<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ConsultResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'medical' => $this->medical->fullName,
            'patient' => new PatientResource($this->patient),
            'start' => (new \Carbon\Carbon($this->start))->format('Y-m-d'),
            'status' => __($this->statu)
        ];
    }
}
