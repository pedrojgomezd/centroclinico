<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use App\Models\Consult;
use Illuminate\Http\Request;
use App\Events\RegisterConsult;
use App\Filters\ConsultsFilters;
use App\Http\Resources\ConsultResource;
use App\Http\Resources\ConsultCollection;

class ConsultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ConsultsFilters $filters)
    {   
        $consults = Consult::filter($filters)
                        ->with(['create_by', 'patient', 'medical', 'medical'])
                        ->orderBy('start');                        
        // if (request()->start == '') {
        //         $consults->where('statu', 'open')
        //                 ->whereDate('start', Carbon::today());
        // }        
        return ConsultResource::collection($consults->paginate(10));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $consult = auth()->user()
            ->consults()
            ->create(request()->all());
        //event(new RegisterConsult($consult));
        return new ConsultResource($consult->fresh());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return tap(User::find($id))->load('my_consults');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Consult $consult)
    {   
        $consult->update(request()->all());
        $consult->load('patient');
        return new ConsultResource($consult);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($consult)
    {
        Consult::destroy($consult);
        return response('Consulta Eliminada');
    }
}
