<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function medicals()
    {
        return $this->json(tap(User::all())->load('profile'));
    }

    public function medical(User $username)
    {
        return $this->json($username->load('profile'));
    }

    public function newConsulta(User $user)
    {
    	return $this->json($user);
    }

    protected function json($value)
    {
    	return response()->json($value);
    }
}
