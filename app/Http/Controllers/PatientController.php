<?php

namespace App\Http\Controllers;

use App\Models\Patient;
use Illuminate\Http\Request;
use App\Http\Resources\PatientResource;
use App\Http\Requests\PatientFormRequest;

class PatientController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('patients.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('patients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatientFormRequest $request)
    {
        $patient = Patient::create($request->all());

        $patient->addAddress($request->all());

        return new PatientResource($patient->fresh());
        //return redirect("/admin/patients/{$patient->id}");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        if (request()->ajax()) {
            return new PatientResource($patient);
        }
        return view('patients.show', compact('patient'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient)
    {
        return view('patients.edit', [
            'patient' => $this->mergeWith($patient)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PatientFormRequest $request, Patient $patient)
    {
        $patient->updateChange($request->all());
        return response()->json(['menssage' => 'Actualizado correctamente']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function avatar(Patient $patient)
    {
        $avatar = request()->avatar;
        $avatar->storeAs('/public/avatars', $patient->id.'.'.$avatar->extension());
        $patient->update(['avatar' => $patient->id.'.'.$avatar->extension()]);
    }

    public function fetech()
    {
        $patient = Patient::with('addresses')->get();
        return PatientResource::collection($patient);
    }

    protected function mergeWith($model)
    {
        return collect($model)->merge($model->address);
    }
}
