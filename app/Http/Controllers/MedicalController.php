<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\MedicalResource;

class MedicalController extends Controller
{
    public function __invoke()
    {
    	$consults = tap(auth()->user()->my_consults)
    					->load('patient');
    	return view('medicals.index')->withConsults($consults);
    }

    public function index()
    {
        return MedicalResource::collection(User::medicals());
    }
}
