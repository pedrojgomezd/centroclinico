<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['phone_home', 'phone_office', 'mobil'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
