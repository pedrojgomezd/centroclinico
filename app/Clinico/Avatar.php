<?php 

namespace Clinico;

use Illuminate\Http\Request;


/**
* Subir avatar
*/
class Avatar
{
	public $request;
	private $model;

	function __construct($request, $model)
	{
		$this->request = $request;
		$this->model = $model;
	}


	public function load()
	{
		if ($this->request) {			
			$name = str_before($this->model->email, '@');
			$path =$this->request
					->storeAs('public/avatars', $name.'.'.$this->request->extension());
			return str_after($path, 'public/avatars/');
		}
		return null;
	}

}