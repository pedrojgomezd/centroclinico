<?php 

namespace Clinico;

use Clinico\Avatar;

trait AvatarTrait {

	public static function bootAvatarTrait()
    {
        static::creating(function($model){
        	$request = request('avatar');
            $model->avatar = (new Avatar($request, $model))->load();
        });

        static::updating(function($model){
        	$request = request('avatar');
            $model->avatar = (new Avatar($request, $model))->load();
        });

    }

    public function getAvatarPathAttribute()
    {
        return $this->avatar ?: 'avatar.jpg';
    }
}