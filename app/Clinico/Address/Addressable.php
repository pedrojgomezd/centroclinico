<?php 

namespace Clinico\Address;


trait Addressable 
{
    
	/**
     * Get all addresses for this model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function addresses()
    {
        return $this->morphMany(Address::class, 'addressable');
    }

    public function getAddressAttribute()
    {
    	return $this->addresses()->first();
    }

    /**
     * Check if model has an address.
     *
     * @return bool
     */
    public function hasAddress()
    {
        return (bool) $this->addresses()->count();
    }

    /**
     * Add an address to this model.
     *
     * @param  array  $attributes
     * @return mixed
     *
     * @throws \Exception
     */
    public function addAddress($attributes)
    {
        return $this->addresses()->create($attributes);
    }

    /**
     * Updates the given address.
     *
     * @param  Address  $address
     * @param  array    $attributes
     * @return bool
     *
     * @throws \Exception
     */
    public function updateAddress(Address $address, array $attributes)
    {
        return $address->fill($attributes)->save();
    }

    /**
     * Deletes given address.
     *
     * @param  Address  $address
     * @return bool
     *
     * @throws \Exception
     */
    public function deleteAddress(Address $address)
    {
        if ($this !== $address->addressable()->first())
            return false;
        return $address->delete();
    }
}