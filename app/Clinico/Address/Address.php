<?php 

namespace Clinico\Address;

use Illuminate\Database\Eloquent\Model;

/**
* Addressable Model
*/
class Address extends Model
{
	 protected $fillable = [
        'country',
        'state',
        'city',
        'post_code',
        'street',
        'number',
        'lat',
        'lng',
        'note',
        'addressable_id',
        'addressable_type',
    ];

    protected $hidden = ['id'];

    /**
     * Get the related model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function addressable()
    {
    	return $this->morphTo();
    }
}