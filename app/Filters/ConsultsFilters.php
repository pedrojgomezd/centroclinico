<?php 

namespace App\Filters;

/**
* Query Filters
*/
class ConsultsFilters extends Filters
{
	
	protected $filters = ['patient', 'medical','statu', 'start'];

	public function patient($id)
	{
		return $this->builder->where('patient_id', $id);
	}

	public function statu($statu = 'open')
	{
		return $this->builder->where('statu', $statu);
	}

	public function medical($medical)
	{
		return $this->builder->where('medical_id', $medical);
	}

	public function start($start)
	{
		return $this->builder->whereDate('start', $start);
	}

}