<?php 

namespace App\Repositories;

use App\Models\Consult;
use App\Repositories\Redis;


/**
* Repositories Consults
*/
class Consults
{
	public function allMonth()
	{
		return Consult::whereMonth('start', '01')->count();
	}

	public function allOpen()
	{
		return Consult::where('statu', 'open')->count();
	}
}
