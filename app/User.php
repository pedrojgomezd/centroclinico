<?php

namespace App;

use Clinico\Address\Addressable;
use Clinico\AvatarTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Intervention\Zodiac\EloquentZodiacTrait;

class User extends Authenticatable
{
    use Notifiable, Addressable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 
        'first_name', 
        'last_name', 
        'email',
        'mobil',
        'phone_home',
        'gender',   
        'profile_id',
        'birthday', 
        'password',
        'roles',
        'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected static function boot()
    {
        parent::boot();
        
        static::creating(function ($model) {
            $model->password = bcrypt('secret');
        });
    }

    public function getRouteKeyName()
    {
        return 'username';
    }

    /**
     * Relaciones
     */
    
    public function profile()
    {
        return $this->hasOne(Profile::class)->withDefault();
    }

    public function consults()
    {
        return $this->hasMany(\App\Models\Consult::class, 'created_by');
    }

    public function my_consults()
    {
        return $this->hasMany(\App\Models\Consult::class, 'medical_id');
    }


    public static function insert($data)
    {
        $user = static::create($data);
        //$user->profile()->create($data);
        $user->addresses()->create($data);
        return $user;
    }

    public function updateChange($request)
    {
        $this->update($request);
        $this->addresses()
            ->first()
            ->update($request);
            
        return $this;
    }

    public function getFullNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function scopeMedicals($query)
    {
        return $query->where('roles', 'medical')->get();
    }

    public function isAdmin()
    {
        return (bool) $this->roles == 'admin';
    }

    public function getAvatarPathAttribute()
    {
        return $this->avatar;
    }
}
