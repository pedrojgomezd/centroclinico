<?php

namespace App\Providers;

use App\Models\Consult;
use App\Repositories\Consults;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Schema::defaultStringLength(191);

        $this->menu();
        $this->consultsResume();

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }

    public function menu()
    {
        $menu = [
            'Dashboard' => [
                'route' => 'reception.dashboard',
                'icon' => 'fa fa-dashboard',
                'class' => 'blue'
            ],
            'Expedientes' => [
                'route' => 'patients.index',
                'icon' => 'fa fa-users',
                'class' => 'gold'
            ],
            'Usuarios y Medicos' => [
                'route' => 'users.index',
                'icon' => 'fa fa-user-md',
                'class' => 'purple',
            ],
            'Agenda' => [
                'route' => 'medicals.agenda',
                'icon' => 'fa fa-calendar',
                'class' => 'green',
            ]
        ];
        
        view()->composer('layouts.partials.nav', function($view) use ($menu){
            $view->withMenu($menu);
        });
    }

    public function consultsResume()
    {
        $consults = new Consults;
        view()->composer('components.header-info.widget-header', function ($view) use ($consults) {
            $view->withConsultsAll($consults->allMonth())
                ->withConsultsOpen($consults->allOpen());
        });
    }
}
