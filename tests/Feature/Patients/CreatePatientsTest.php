<?php

namespace Tests\Feature\Patients;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreatePatientsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_create_a_new_patient()
    {
        $response = $this->get('/admin/patients/create')
        	->assertSee('hPacientes');	
    }
}
	